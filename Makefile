.POSIX:

CC = tcc
CFLAGS = -Os
EXE = colortest
SRC = colortest.c
PREFIX = ~/.local

all: $(SRC)
	$(CC) -o $(EXE) $(SRC) $(CFLAGS)

install: $(EXE)
	cp $(EXE) $(PREFIX)/bin

clean: $(EXE)
	rm -f $(EXE)
