#include <stdio.h>

/*
 * colortest
 *
 * Program for testing the base 16 colors of
 * a terminal emulator.
 *
 * https://gitlab.com/J0ND03/colortest.git
 *
 */
/*
 *	- array with colors
 *	- print all struct colors
 */

int
main()
{
	/* First 8 colors are normal colors, last 8 bright.  */
	printf("normal: ");

	/* Loop through 0-15 printing each terminal color.  */
	for (int x = 0; x < 16; x++){
		/* Change foreground to white for dark colors like 0 and 8.  */
		int fg;

		if (!x | (x == 8)){
			fg = 37;
		} else /* otherwise, fg is black */
			fg = 30;

		/* Print formatted color unit.  */
		printf("\033[%i;48;5;%im %-3i", fg, x, x);

		/* Newline once bright colors are reached.  */
		if (x == 7)
			printf("\033[0m\n\nbright: ");
	}

    /* Reset colors once program finishes.  */
    printf("\033[0m\n");

    return 0;
}
